package com.ankitnandwani.cucumber.StepDefs;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * Created by mac on 25/05/17.
 */
public class search {

    @Given("^I'm at duckduckgo homepage$")
    public void imAtDuckduckgoHomepage(){
        System.out.println("Open DuckDuckGo.com");

    }

    @When("^I enter (.*) in search bar$")
    public void iEnterInSearchBar(String keyword){
        System.out.println("Enter Keyword : " + keyword);

    }

    @Then("^Search results should be displayed$")
    public void searchResultsShouldBeDisplayed(){
        System.out.println("Results Displayed");

    }
}
